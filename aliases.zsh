alias vi=nvim
alias la="ls -a"
alias ll="ls -l"
# Git Aliases
alias gits="git status"
alias gclean="git clean"
alias gitl="git log"
alias gitc="git commit"
alias gitm="git merge"
alias pull="git pull"
alias push="git push"
alias gitwf="git merge origin/staging monde.ngalonkulu"
#Gradle
alias gradlew="./gradlew"
alias gw="./gradlew"
alias wclean="./gradlew clean"
alias wscln="wclean; ./gradlew cleanVaadinThemeCompile"
alias wref="./gradlew clean --refresh-dependencies --stacktrace -i"
alias wvtc="./gradlew vaadinThemeCompile -x build"
alias wbld="./gradlew build -x test -x javadoc --info --stacktrace"
alias wsbld="wbld; wvtc"
alias wrbld="wscln; wsbld"
alias wrun="./gradlew run"
alias wrrun="wclean; wrun"
#Docker Composer:
alias dup="docker-compose -f docker/ide/docker-compose.yml up"
alias dub="docker-compose -f docker/ide/docker-compose.yml up --build"
alias drub="docker-compose -f docker/ide/docker-compose.yml rm; docker-compose -f docker/ide/docker-compose.yml up --build"
alias drm="docker-compose -f docker/ide/docker-compose.yml rm"
alias dkide="docker-compose -f docker/ide/docker-compose.yml"
alias dexec="docker exec -it"
# My Workflow:
# I use this to list my recent commits (in case I forgot to log hours)
alias gitme="git log | grep -A 4 'Author: Monde' | head -n 256 | less"
